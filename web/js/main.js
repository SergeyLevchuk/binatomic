$(document).ready(function () {

    $('body').on('pjax:end', '#login, #signup', function () {
        $.pjax.reload({container: "#widget"})
    });
    $('.login ').on('click', function () {
        selectForm();
    });

    $('.sign-up').on('click', function () {
        selectForm();
    })
});

function selectForm() {
    $('.form-menu-item').toggleClass('active');
    $('.form-item').toggleClass('active');
    $('.alert').toggleClass('non-active');
}
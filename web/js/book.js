$(document).ready(function () {
    let body = $('body');

    body.on('pjax:end', '#pjax-create-book, #pjax-update-book', function () {
        $.pjax.reload({container: "#widget"});
        $.pjax.reload({container: "#grid-books"});
        $('#modal-create').modal('hide');
    });

    body.on('click', '.pjax-delete-link', function (e) {
        e.preventDefault();
        var deleteUrl = $(this).attr('delete-url');
        var pjaxContainer = $(this).attr('pjax-container');
        var result = confirm('Delete this item, are you sure?');
        if (result) {
            $.ajax({
                url: deleteUrl,
                type: 'post',
                error: function (xhr) {
                    alert('There was an error with your request.' + xhr.responseText);
                }
            }).done(function (data) {
                $.pjax.reload({container: "#widget"});
                $.pjax.reload({container: "#grid-books"});
            });
        }
    });


    body.on('click', '.update-modal-click', function () {
        $('#update-modal')
            .modal('show')
            .find('#updateModalContent')
            .load($(this).attr('value'));
    });

    body.on('click', '.full-image', function (e) {
        e.preventDefault();
        $('#fullImage')
            .modal('show')
            .find('#viewFullImage')
            .load($(this).attr('href'));
    });
});
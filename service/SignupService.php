<?php

namespace app\service;

use app\models\SignupForm;
use app\models\User;
use Yii;

/**
 * Class SignupService
 * @package app\service
 */
class SignupService
{
    /**
     * @param SignupForm $form
     * @throws \yii\base\Exception
     */
    public function signup(SignupForm $form)
    {
        $user = new User();
        $user->generateToken();
        $user->setPassword($form->password);
        $user->email = $form->email;
        $user->status = User::STATUS_WAIT;

        if (!$user->save()) {
            throw new \RuntimeException('Saving error');
        }
        $this->sentEmailConfirm($user);
    }

    /**
     * @param User $user
     */
    private function sentEmailConfirm(User $user)
    {
        $sent = Yii::$app->mailer
            ->compose(
                ['html' => 'user-signup-confirm-html', 'text' => 'user-signup-confirm-text'],
                ['user' => $user])
            ->setTo($user->email)
            ->setFrom(Yii::$app->params['adminEmail'])
            ->setSubject('Confirmation of registration')
            ->send();

        if (!$sent) {
            throw new \RuntimeException('Sending error');
        }
    }

    /**
     * @param $token
     */
    public function confirmation($token): void
    {
        if (empty($token)) {
            throw new \DomainException('Empty confirm token');
        }

        $user = User::findOne(['token' => $token]);
        if (!$user) {
            throw new \DomainException('User is not found');
        }

        $user->status = User::STATUS_ACTIVE;
        if (!$user->save()) {
            throw new \RuntimeException('Saving error');
        }

        if (!Yii::$app->getUser()->login($user)) {
            throw new \RuntimeException('Error authentication');
        }
    }
}
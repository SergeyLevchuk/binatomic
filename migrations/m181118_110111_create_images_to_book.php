<?php

use yii\db\Migration;

/**
 * Class m181118_110111_create_images_to_book
 */
class m181118_110111_create_images_to_book extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        $this->createTable('{{%images}}', [
            'id' => $this->primaryKey(),
            'default_name' => $this->string(255)->notNull(),
            'save_name' => $this->string(255)->notNull(),
            'addresses_book_id' => $this->integer()->notNull()
        ]);

        $this->addForeignKey('images_addresses_book', 'images', 'addresses_book_id',
            'addresses_book', 'id', 'CASCADE');

    }

    /**
     * {@inheritdoc}
     */
    public function down()
    {
        $this->dropTable('images');
    }

}

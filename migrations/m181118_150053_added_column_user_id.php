<?php

use yii\db\Migration;

/**
 * Class m181118_150053_added_column_user_id
 */
class m181118_150053_added_column_user_id extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        $this->addColumn('addresses_book', 'user_id', $this->integer()->notNull());


        $this->addForeignKey('users_addresses_book', 'addresses_book', 'user_id',
            'user', 'id', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function down()
    {
        $this->dropForeignKey('users_addresses_book', 'addresses_book');
        $this->dropColumn('addresses_book', 'user_id');
    }


}

<?php

use yii\db\Migration;

/**
 * Class m181117_123556_create_table_users
 */
class m181117_123556_create_table_users extends Migration
{
    /**
     * @{inheritdoc}
     */
    public function up()
    {
        $this->createTable('{{%user}}', [
            'id' => $this->primaryKey(),
            'token' => $this->string(32)->notNull(),
            'password_hash' => $this->string()->notNull(),
            'email' => $this->string()->notNull()->unique(),
            'status' => $this->smallInteger()->notNull()->defaultValue(10),
        ], 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB');
    }

    /**
     * @{inheritdoc}
     */
    public function down()
    {
        $this->dropTable('{{%user}}');
    }
}

<?php

use yii\db\Migration;

/**
 * Class m181118_105707_create_addresses_book
 */
class m181118_105707_create_addresses_book extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        $this->createTable('{{%addresses_book}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(25)->notNull(),
            'phone' => $this->string(25)->notNull(),
            'email' => $this->string(25)->notNull(),
            'address' => $this->string(255)->notNull()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function down()
    {
        $this->dropTable('addresses_book');
    }

}

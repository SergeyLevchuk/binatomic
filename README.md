BitAtomic
------
This is a test project to a company BinAtomic. Project is build with the help of [Yii2](http://www.yiiframework.com/)  framework.


Installation
------------

This article describe installation of web application using vagrant.
This installation way does not require pre-installed software (such as web-server, PHP, MySQL etc.) - just do next steps!

#### Windows

1. Install [Vagrant](https://www.vagrantup.com/downloads.html)
2. Install [VirtualBox](https://www.virtualbox.org/wiki/Downloads)
3. Reboot your machine
4. Open terminal (`cmd.exe`)
5. Run `vagrant plugin install vagrant-bindfs`
6. Run `vagrant plugin install vagrant-disksize`
7. Run `vagrant plugin install vagrant-hostmanager`
8. Run `vagrant plugin install vagrant-hostsupdater`
9. Run `vagrant plugin install vagrant-share`
10. Run `vagrant plugin install vagrant-triggers`
11. Run `vagrant plugin install vagrant-vbguest`
12. **Change directory to project root**
13. Copy `vagrant-local.example.yml` to `vagrant-local.yml` inside folder `vagrant/config`. Generate `github_token` to `vagrant-local.yml` on [GitHub](https://github.com)
14. In terminal(`cmd.exe`) run `vagrant up`
15. If your run `vagrant up` there was an error `timeout waiting for machine to boot...` you need enable Virtualization Technology in BIOS

#### Linux/Unix

Description will add later

#####Default MySql credentials:
 
For local version:<br>

Login: binatomic<br>
Password: binatomic

For production need to set in folder environments


Launching after installation
----------------------------
#### Windows

1. Open terminal (`cmd.exe`)
2. Change directory to project root
3. Run `vagrant up`
4. If you need stop vagrant, in terminal(`cmd.exe`) run `vagrant halt`

#### Linux/Unix

Description will add later
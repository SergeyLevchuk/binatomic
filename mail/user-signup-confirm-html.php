<?php

use yii\helpers\Html;

/* @var $user \app\models\User */

$confirmLink = Yii::$app->urlManager->createAbsoluteUrl(['site/signup-confirm', 'token' => $user->token]);
?>
<div class="password-reset">

    <p>Follow the link below to confirm your email:</p>

    <p><?= Html::a(Html::encode('Click here'), $confirmLink) ?></p>
</div>
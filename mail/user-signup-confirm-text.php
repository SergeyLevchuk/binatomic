<?php

/* @var $user \app\models\User */

$confirmLink = Yii::$app->urlManager->createAbsoluteUrl(['site/signup-confirm', 'token' => $user->token]);
?>

    Follow the link below to confirm your email:

<?= $confirmLink ?>
<?php

namespace app\controllers;


use app\models\LoginForm;
use app\models\SignupForm;
use app\service\SignupService;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\Response;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'only' => ['index', 'login', 'logout', 'signup', 'signup-confirm'],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index', 'login', 'signup', 'signup-confirm'],
                        'roles' => ['?'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['index', 'logout'],
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'logout' => ['post'],
                    'singup-confirm' => ['get'],
                    'signup' => ['post'],
                    'login' => ['post']
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->redirect(['addresses-book/index']);
        }
        $loginFormModel = new LoginForm();
        $signUpFormModel = new SignupForm();

        return $this->render('index', [
            'loginFormModel' => $loginFormModel,
            'signUpFormModel' => $signUpFormModel
        ]);
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post())) {
            try {
                if ($model->login()) {
                    return $this->redirect('index');
                }
            } catch (\DomainException $e) {
                Yii::$app->session->setFlash('error', $e->getMessage());
                return $this->render('_login', [
                    'model' => $model,
                ]);
            }
        }

        return $this->render('_login', [
            'model' => $model
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * @return string|Response
     * @throws \yii\base\Exception
     */
    public function actionSignup()
    {
        $model = new SignupForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $signupService = new SignupService();

            try {
                $signupService->signup($model);
                Yii::$app->session->setFlash('success', 'Check your email to confirm the registration');

                return $this->render('_signup', [
                    'model' => $model,
                ]);
            } catch (\RuntimeException $e) {
                Yii::$app->errorHandler->logException($e);
                Yii::$app->session->setFlash('error', $e->getMessage());
            }
        }

        return $this->render('_signup', [
            'model' => $model,
        ]);
    }

    /**
     * @param $token
     * @return Response
     */
    public function actionSignupConfirm($token)
    {
        $signupService = new SignupService();

        try {
            $signupService->confirmation($token);
            Yii::$app->session->setFlash('success', 'You have successfully confirmed your registration');
        } catch (\Exception $e) {
            Yii::$app->errorHandler->logException($e);
            Yii::$app->session->setFlash('error', $e->getMessage());
        }

        return $this->goHome();
    }
}

<?php

namespace app\controllers;

use app\models\Images;
use yii\filters\AccessControl;
use yii\web\Controller;

class ImagesController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'only' => ['get-full-image'],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['get-full-image'],
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actionGetFullImage($name)
    {
        $model = new Images();
        $img = $model->getFullImage($name);

        return $this->renderAjax('full-image', ['img' => $img]);
    }
}
<?php

namespace app\models;

use yii\base\Model;

/**
 * Class SignupForm
 * @package app\models
 */
class SignupForm extends Model
{
    public $email;
    public $password;
    public $password_repeat;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['email', 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            ['email', 'unique', 'targetClass' => '\app\models\User',
                'message' => "User with this email already exists"],

            ['password', 'required'],
            ['password', 'string', 'min' => 6],

            ['password_repeat', 'required'],
            ['password_repeat', 'compare', 'compareAttribute' => 'password', 'message' => "Passwords does not match"],
        ];
    }

    /**
     *  {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'password' => 'Password',
            'password_repeat' => 'Repeat password',
            'email' => 'E-mail'
        ];
    }
}
<?php

namespace app\models;

use app\helpers\ImgHelper;
use app\repository\ImagesRepository;
use yii\helpers\Html;

/**
 * This is the model class for table "images".
 *
 * @property int $id
 * @property string $default_name
 * @property string $save_name
 * @property int $addresses_book_id
 *
 * @property AddressesBook $addressesBook
 */
class Images extends \yii\db\ActiveRecord
{

    const PREVIEW_IMG_DIR = 'preview';
    const ORIGINAL_IMG_DIR = 'original';
    const MAX_UPLOAD_IMG = 5;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'images';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['default_name', 'save_name', 'addresses_book_id'], 'required'],
            [['addresses_book_id'], 'integer'],
            [['default_name', 'save_name'], 'string', 'max' => 255],
            [['addresses_book_id'], 'exist', 'skipOnError' => true, 'targetClass' => AddressesBook::class,
                'targetAttribute' => ['addresses_book_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'default_name' => 'Default Name',
            'save_name' => 'Save Name',
            'addresses_book_id' => 'Addresses Book ID',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            $repository = new ImagesRepository();
            $count = $repository->countUploadedImages($this->addresses_book_id);
            if ($count >= self::MAX_UPLOAD_IMG) {
                $this->deleteImage($this->attributes);
                throw new \DomainException('You have exceeded the limit of images per user.');
            }
            return true;
        }
        return false;
    }

    /**
     *{@inheritdoc}
     */
    public function beforeDelete()
    {
        $this->deleteImage($this);
        return parent::beforeDelete();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAddressesBook()
    {
        return $this->hasOne(AddressesBook::class, ['id' => 'addresses_book_id']);
    }

    /**
     * @param array $images
     * @param string $id
     * @return bool
     */
    public static function addImages(array $images, string $id)
    {
        foreach ($images as $img) {
            $model = new self();
            $model->save_name = $img['saveName'];
            $model->default_name = $img['defaultName'];
            $model->addresses_book_id = $id;
            if (!$model->save()) {
                return false;
            }
        }
        return true;
    }

    /**
     * @param $id
     * @return array
     */
    public static function getPreview($id): array
    {
        $repository = new ImagesRepository();
        $images = $repository->getImagesToUserAsArray($id);
        $result = [];
        foreach ($images as $img) {
            $result[] = Html::img('images' . '/' . Images::PREVIEW_IMG_DIR . '/' . $img['save_name']);
        }
        return $result;
    }

    public function getFullImage(string $name): string
    {
        return 'images' . '/' . Images::ORIGINAL_IMG_DIR . '/' . $name;
    }

    /**
     * @param ImgHelper $helper
     * @param $img
     */
    private function deleteImage($img)
    {
        $helper = new ImgHelper();
        $helper->deleteImg($img->save_name);
    }
}

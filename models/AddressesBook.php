<?php

namespace app\models;

use app\helpers\ImgHelper;
use Yii;

/**
 * This is the model class for table "addresses_book".
 *
 * @property int $id
 * @property string $name
 * @property string $phone
 * @property string $email
 * @property string $address
 * @property integer $user_id
 * @property Images[] $images
 */
class AddressesBook extends \yii\db\ActiveRecord
{
    public $img;
    private $files;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'addresses_book';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'phone', 'email', 'address'], 'required'],
            [['img'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg', 'maxFiles' => 5],
            [['name', 'phone', 'email'], 'string', 'max' => 25],
            [['email'], 'email'],
            [['address'], 'string', 'max' => 255],
            [['user_id'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'phone' => 'Phone',
            'email' => 'Email',
            'address' => 'Address',
            'img' => 'Img'
        ];
    }

    /**
     *{@inheritdoc}
     */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            $this->user_id = Yii::$app->user->id;
            return true;
        }
        return false;
    }

    /**
     *{@inheritdoc}
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        Images::addImages($this->files, $this->id);
    }

    /**
     *{@inheritdoc}
     */
    public function beforeDelete()
    {
        $this->deletedImages($this->id);
        return parent::beforeDelete();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getImages()
    {
        return $this->hasMany(Images::class, ['addresses_book_id' => 'id']);
    }

    /**
     * @throws \yii\base\Exception
     */
    public function uploadFile()
    {
        $helper = new ImgHelper();
        $this->files = $helper->uploadImg($this, 'img');
    }

    /**
     * @param $id
     */
    public function deletedImages($id)
    {
        $images = Images::findAll(['addresses_book_id' => $id]);
        foreach ($images as $img) {
            $img->delete();
        }
    }
}

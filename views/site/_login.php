<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */

/* @var $model app\models\LoginForm */

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\widgets\Pjax;

?>

<div class="form-item form-login-wrapp active">

    <?php Pjax::begin([
        'id' => 'login',
        'enablePushState' => false,
    ]) ?>

    <?php $form = ActiveForm::begin([
        'id' => 'login-form',
        'layout' => 'horizontal',
        'action' => 'site/login',
        'fieldConfig' => [
            'template' => '{input}{error}',
        ],
        'options' => [
            'data-pjax' => true
        ]
    ]); ?>

    <?= $form->field($model, 'email')->textInput([
        'autofocus' => true,
        'placeholder' => $model->getAttributeLabel('email')
    ]) ?>

    <?= $form->field($model, 'password')->passwordInput([
        'placeholder' => $model->getAttributeLabel('password')
    ]) ?>

    <div class="form-group">
        <?= Html::submitButton('Login', ['class' => 'btn btn-primary col-md-12', 'name' => 'login-button']) ?>
    </div>

    <?php ActiveForm::end(); ?>

    <?php Pjax::end() ?>
</div>

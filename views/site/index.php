<?php

/* @var $this yii\web\View */
/* @var $loginFormModel app\models\LoginForm */

/* @var $signUpFormModel app\models\SignupForm */

use app\assets\LoginAsset;
use app\widgets\Alert;
use yii\widgets\Pjax;

$this->title = Yii::$app->name;
LoginAsset::register($this);
?>
<div class="site-index">

    <section id="form">
        <div class="container">
            <div class="form-menu">
                <span class="form-menu-item login active">Login</span>
                <span class="form-menu-item sign-up">Sign up</span>
            </div>
            <div class="form-wrapp">
                <?php Pjax::begin(['id' => 'widget']) ?>
                <?= Alert::widget() ?>
                <?php Pjax::end() ?>
                <?= $this->render('_login', ['model' => $loginFormModel]); ?>

                <?= $this->render('_signup', ['model' => $signUpFormModel]); ?>
            </div>
        </div>
    </section>
</div>

<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */

/* @var $model app\models\SignupForm */

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\widgets\Pjax;

?>

<div class="form-item form-sing-up-wrapp">

    <?php Pjax::begin([
        'id' => 'signup',
        'enablePushState' => false,
    ]) ?>

    <?php $form = ActiveForm::begin([
        'id' => 'signup-form',
        'layout' => 'horizontal',
        'action' => 'site/signup',
        'fieldConfig' => [
            'template' => '{input}{error}',
        ],
        'options' => [
            'data-pjax' => true
        ]
    ]); ?>

    <?= $form->field($model, 'email')->textInput([
        'placeholder' => $model->getAttributeLabel('email')
    ]) ?>

    <?= $form->field($model, 'password')->passwordInput([
        'placeholder' => $model->getAttributeLabel('password')
    ]) ?>

    <?= $form->field($model, 'password_repeat')->passwordInput([
        'placeholder' => $model->getAttributeLabel('password_repeat')
    ]) ?>

    <div class="form-group">
        <?= Html::submitButton('Singup', ['class' => 'btn btn-primary col-md-12', 'name' => 'login-button']) ?>
    </div>

    <?php ActiveForm::end(); ?>

    <?php Pjax::end() ?>
</div>

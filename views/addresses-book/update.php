<?php

/* @var $this yii\web\View */
/* @var $model app\models\AddressesBook */

?>
<div class="addresses-book-update">

    <?= $this->render('_form_update', [
        'model' => $model,
    ]) ?>

</div>

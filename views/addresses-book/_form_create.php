<?php

use kartik\file\FileInput;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model app\models\AddressesBook */
/* @var $form yii\widgets\ActiveForm */
/* @var $action string */

?>

<div class="addresses-book-form">

    <?php Pjax::begin([
        'id'=> 'pjax-create-book',
        'enablePushState' => false
    ]) ?>

    <?php $form = ActiveForm::begin([
        'action' => 'create',
        'id'=> 'form-create',
        'options' => [
            'data-pjax' => true
        ]
    ]); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'phone')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'address')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'img[]')->widget(FileInput::class, [
        'options' => [
            'multiple' => true,
            'id'=> 'create-files'
        ],
        'pluginOptions' => [
            'showCaption' => false,
            'maxFileCount' => 5
        ]
    ]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

    <?php Pjax::end(); ?>

</div>

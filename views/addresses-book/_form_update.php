<?php

use app\models\Images;
use kartik\file\FileInput;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model app\models\AddressesBook */

?>
<div class="addresses-book-update">

    <?php Pjax::begin([
        'id' => 'pjax-update-book',
        'enablePushState' => false
    ]) ?>

    <?php $form = ActiveForm::begin([
        'action' => "addresses-book/update/?id={$model->id}",
        'id' => 'form-update',
        'options' => [
            'data-pjax' => true
        ]
    ]); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'phone')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'address')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'img[]')->widget(FileInput::class, [
        'options' => [
            'multiple' => true,
            'id' => 'update-files'
        ],
        'pluginOptions' => [
            'showCaption' => false,
            'initialPreview' => Images::getPreview($model->id),
            'maxFileCount' => 5
        ]
    ]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

    <?php Pjax::end(); ?>

</div>

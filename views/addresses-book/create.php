<?php

use yii\bootstrap\Modal;


/* @var $this yii\web\View */
/* @var $model app\models\AddressesBook */

?>
<div class="addresses-book-create">

    <?php
    Modal::begin([
        'header' => "<h3>$this->title</h3>",
        'toggleButton' => [
            'label' => 'Add Addresses Book',
            'class' => 'btn btn-success'

        ],
        'size' => 'modal-lg',
        'options' => [
            'id' => 'modal-create'
        ]
    ]);

    echo $this->render('_form_create', [
        'model' => $model
    ]);

    Modal::end();
    ?>

</div>

<?php

use app\assets\BookAsset;
use app\models\Images;
use app\widgets\Alert;
use kartik\grid\GridView;
use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\models\AddressesBookSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Addresses Book';
$this->params['breadcrumbs'][] = $this->title;
BookAsset::register($this);
?>
    <div class="addresses-book-index">

        <h1><?= Html::encode($this->title) ?></h1>

        <?= $this->render('create', ['model' => $model]) ?>

        <?php Pjax::begin(['id' => 'widget']) ?>
        <?= Alert::widget() ?>
        <?php Pjax::end() ?>

        <?= GridView::widget([
            'summary' => false,
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'pjax' => true,
            'pjaxSettings' => [
                'neverTimeout' => true,
                'options' => [
                    'id' => 'grid-books'
                ],
                'enablePushState' => false
            ],
            'columns' => [
                [
                    'attribute' => 'name',
                    'vAlign' => 'middle',
                    'hAlign' => 'center'
                ],
                [
                    'attribute' => 'phone',
                    'vAlign' => 'middle',
                    'hAlign' => 'center'
                ],
                [
                    'attribute' => 'email',
                    'vAlign' => 'middle',
                    'hAlign' => 'center'
                ],
                [
                    'attribute' => 'address',
                    'vAlign' => 'middle',
                    'hAlign' => 'center'
                ],
                [
                    'label' => 'Images',
                    'headerOptions' => ['class' => 'kv-align-center'],
                    'format' => 'raw',
                    'width' => '620px',
                    'value' => function ($model) {
                        $images = null;
                        foreach ($model->images as $img) {
                            $images .= Html::a(Html::img('images/' . Images::PREVIEW_IMG_DIR . '/' . $img->save_name, [
                                'class' => 'img-preview'
                            ]), ['images/get-full-image', 'name' => $img->save_name], ['class' => 'full-image']);;
                        }
                        return $images;
                    }
                ],
                [
                    'class' => 'kartik\grid\ActionColumn',
                    'vAlign' => 'middle',
                    'template' => '{update} {delete}',
                    'buttons' => [
                        'update' => function ($url) {
                            $btn = Html::button("<span class='glyphicon glyphicon-pencil'></span>", [
                                'value' => $url,
                                'class' => 'update-modal-click grid-action',
                                'data-toggle' => 'tooltip',
                                'data-placement' => 'bottom',
                                'title' => 'Update'
                            ]);
                            return $btn;
                        },
                        'delete' => function ($url) {
                            return Html::a('<span class="glyphicon glyphicon-trash"></span>', false, [
                                'class' => 'pjax-delete-link',
                                'delete-url' => $url,
                                'pjax-container' => 'my_pjax',
                                'title' => Yii::t('yii', 'Delete')
                            ]);
                        },
                    ],
                ],
            ],
        ]); ?>
    </div>

<?php
Modal::begin([
    'header' => '<h4>Update</h4>',
    'id' => 'update-modal',
    'size' => 'modal-lg'
]);

echo "<div id='updateModalContent'></div>";

Modal::end();
?>

<?php
Modal::begin([
    'header' => '<h4>Image</h4>',
    'id' => 'fullImage',
    'size' => 'modal-lg'
]);

echo "<div id='viewFullImage'></div>";

Modal::end();
?>
<?php

use himiklab\thumbnail\EasyThumbnailImage;

/* @var $img string */

?>

<?= EasyThumbnailImage::thumbnailImg(
    Yii::$app->request->getHostInfo() . '/' . $img,
    600,
    600,
    EasyThumbnailImage::THUMBNAIL_INSET_BOX
); ?>

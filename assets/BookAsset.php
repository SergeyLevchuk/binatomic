<?php

namespace app\assets;

class BookAsset extends AppAsset
{
    public $js = [
        'js/book.js'
    ];
}
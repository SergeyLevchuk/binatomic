<?php

namespace app\assets;


class LoginAsset extends AppAsset
{
    
    public $js = [
        'js/main.js'
    ];
}
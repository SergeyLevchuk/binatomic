<?php

namespace app\helpers;

use app\models\Images;
use Yii;
use yii\base\Model;
use yii\imagine\Image;
use yii\web\UploadedFile;

class ImgHelper
{
    /**
     * @param $model
     * @param $attribute
     * @return array
     * @throws \yii\base\Exception
     */
    public function uploadImg(Model $model, string $attribute): array
    {
        $result = [];
        $this->checkDirImg();
        $files = UploadedFile::getInstances($model, $attribute);
        foreach ($files as $key => $file) {
            $result[] = $this->save($file);
        }
        return $result;
    }

    /**
     * @param $file
     * @return array
     * @throws \yii\base\Exception
     */
    private function save($file)
    {
        $result = [];
        $saveName = $this->generatedFileName($file->extension);

        if (!$file->saveAs($this->getFullImg($saveName))) {
            throw new \DomainException('Images not saved, try again');
        };

        if (!Image::thumbnail($this->getFullImg($saveName), 100, 100)->save($this->getPreviewImg($saveName),
            ['quality' => 80])) {
            throw new \DomainException('Images not saved, try again');
        };

        $result['defaultName'] = $file->name;
        $result['saveName'] = $saveName;

        return $result;
    }

    /**
     * Check exists dir
     */
    private function checkDirImg()
    {
        $this->checkDir($this->getImgFullAlias());
        $this->checkDir($this->getImgPreviewAlias());
    }

    /**
     * @param $dir
     */
    private function checkDir(string $dir)
    {
        if (!file_exists($dir)) {
            mkdir($dir, 0775, true);
        }
    }

    /**
     * @param $extension
     * @return string
     * @throws \yii\base\Exception
     */
    private function generatedFileName(string $extension): string
    {
        return Yii::$app->security->generateRandomString() . time() . '.' . $extension;
    }

    /**
     * @return string
     */
    public function getImgFullAlias(): string
    {
        return $this->getImgAlias() . '/' . Images::ORIGINAL_IMG_DIR;
    }

    /**
     * @return string
     */
    public function getImgPreviewAlias(): string
    {
        return $this->getImgAlias() . '/' . Images::PREVIEW_IMG_DIR;
    }

    /**
     * @return bool|string
     */
    private function getImgAlias(): string
    {
        return Yii::getAlias('@images');
    }

    /**
     * @param $name
     * @return string
     */
    private function getFullImg(string $name): string
    {
        return $this->getImgFullAlias() . '/' . $name;
    }

    /**
     * @param $name
     * @return string
     */
    private function getPreviewImg(string $name): string
    {
        return $this->getImgPreviewAlias() . '/' . $name;
    }

    /**
     * @param $name
     */
    public function deleteImg($name)
    {
        @unlink($this->getFullImg($name));
        @unlink($this->getPreviewImg($name));
    }
}
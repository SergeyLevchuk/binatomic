<?php

namespace app\repository;

use app\models\Images;

class ImagesRepository extends Images
{
    /**
     * @param string $id
     * @return int|string
     */
    public function countUploadedImages(string $id)
    {
        return Images::find()
            ->where(['addresses_book_id' => $id])
            ->count();
    }

    /**
     * @param string $user
     * @return array|\yii\db\ActiveRecord[]
     */
    public function getImagesToUserAsArray(string $user)
    {
        return $this->getImagesToUser($user)
            ->asArray()
            ->all();
    }

    /**
     * @param string $user
     * @return \yii\db\ActiveQuery
     */
    private function getImagesToUser(string $user)
    {
        return Images::find()
            ->where(['addresses_book_id' => $user]);
    }
}